"""Utilities for dataset generation or retrieval"""
import random

import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.datasets import make_blobs


def gen_other(plot=False):
    """This method is not yet finalized, so please don't use it.
    Generate things got from
    See: https://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html
    """
    np.random.seed(0)

    # ============
    # Generate datasets. We choose the size big enough to see the scalability
    # of the algorithms, but not too big to avoid too long running times
    # ============
    n_samples = 1500
    noisy_circles = datasets.make_circles(n_samples=n_samples, factor=.5,
                                          noise=.05)
    noisy_moons = datasets.make_moons(n_samples=n_samples, noise=.05)
    blobs = datasets.make_blobs(n_samples=n_samples, random_state=8)
    no_structure = np.random.rand(n_samples, 2), None

    # Anisotropicly distributed data
    random_state = 170
    X, y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    transformation = [[0.6, -0.6], [-0.4, 0.8]]
    X_aniso = np.dot(X, transformation)
    aniso = (X_aniso, y)



def gen_chess2(plot=False):
    """Generates a chess2 dataset. This method is not yet finalized, so please don't use it.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset, ndarray of shape {1320,2}
    labels : {array like} of int, or shape {1320}
        Contains the true labels of the generated dataset.
    """
    np.random.seed(0)
    random_state = 170
    rows=5
    columns=5
    points=     [ 30, 30, 30, 60, 60,
                  60, 60, 30, 30, 30,
                  90, 90,120,120,  0,
                  30, 30, 30, 60, 60,
                  90, 90, 30, 30, 30]
    clusters=   [  0,  0,  0,  1,  1,
                   2,  2,  0,  0,  0,
                   3,  3,  4,  4,  8,
                   5,  5,  5,  6,  6,
                   7,  7,  5,  5,  5]
    data=np.zeros(shape=(sum(points),2),dtype=float)
    labels=np.zeros(shape=sum(points),dtype=int)
    generated_data=0
    for i in range(rows):
        for j in range(columns):
            block_data = np.random.rand(points[columns*i+j], 2)
            block_labels= np.full(shape=points[columns*i+j],fill_value=clusters[columns*i+j]) #[columns*i+j]*points(columns*i+j)

            for l in range(len(block_data)):
                data[generated_data+l]=block_data[l]+(i,j)
                labels[generated_data+l]=block_labels[l]
            generated_data = generated_data + len(block_data)

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0,c=labels)
        ax.set_title("Chess2 Dataset")
        fig.show()
    return data,labels

def gen_chess(plot=False):
    """Generates the chess dataset.

    A chess dataset is composed by nine squares of random-generated points. Each square hosts a different number of
    points, thus each square is expected to have a different average density.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset, ndarray of shape {1600,2}
    labels : {array like} of int, or shape {1600}
        Contains the true labels of the generated dataset.
    """
    # blobs with varied variances
    np.random.seed(0)
    random_state = 170
    rows=3
    columns=3
    points=[  0, 100,200,
            300,400,300,
             100,300,  0]
    data=np.zeros(shape=(sum(points),2),dtype=float)
    labels=np.zeros(shape=sum(points),dtype=int)
    generated_data=0
    for i in range(rows):
        for j in range(columns):
            block_data = np.random.rand(points[columns*i+j], 2)
            block_labels= np.full(shape=points[columns*i+j],fill_value=(columns*i+j) ) #[columns*i+j]*points(columns*i+j)

            for l in range(len(block_data)):
                data[generated_data+l]=block_data[l]+(i,j)
                labels[generated_data+l]=block_labels[l]
            generated_data = generated_data + len(block_data)

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0,c=labels)
        ax.set_title("Chess Dataset")
        fig.show()
    return data,labels

def gen_ord_chess_scattered(plot=False):
    """Generates an ord_chess_scattered (ordere chess) dataset

    A chess dataset is composed by for squares of points. Each square hosts a different number of points, thus each
    square is expected to have a different average density. The points of each square are disposed in an ordered grid.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : ndarray of shape {618,2}, the generated dataset
    labels : {array like} of int, or shape {618}
        Contains the true labels of the generated dataset.
    """

    random.seed(127,version=2)

    rows=3
    columns=3
    points=[10, 5, 7,
            7,14,10,
            5,7,5]
    points_number=sum([x*x for x in points])
    data=np.zeros(shape=(points_number,2),dtype=float)
    labels=np.zeros(shape=points_number,dtype=int)
    generated_data=0
    cluster=0
    i=0
    j=0
    scatt=0.50 #scatterness
    for i in range(rows):
        for j in range(columns):
            for ii in [x*1.0/points[columns*i+j] for x in range(points[columns*i+j])]:
                for jj in [x * 1.0 / points[columns * i + j] for x in range(points[columns * i + j])]:
                    delta_scatt=scatt/points[columns * i + j]
                    scatt_i=random.random()*delta_scatt - delta_scatt/2.0
                    scatt_j=random.random()*delta_scatt - delta_scatt/2.0
                    data[generated_data]=(i+ii+scatt_i,j+jj+scatt_j)
                    labels[generated_data]=cluster
                    generated_data=generated_data+1
            cluster=cluster+1

    #random è casuale tra 0 e 1
    # (x',y') -< (x+random*0.05,y+random*0.05)
    # (x',y') -< (x+d(x)*random*0.05,y+d(y)*random*0.05)
    # (x',y') -< (x + x*random*0.05,y+y*random*0.05)
    # x,y =1,1
    # x,y= 100,100 <- x' y'= 100+-5,100+-5
    # x',y'= 1+-0.05,1+-0.05

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0,c=labels)
        ax.set_title("ord_chess_scattered Dataset")
        fig.show()
    return data,labels

def gen_ord_chess(plot=False):
    """Generates an ord_chess (ordere chess) dataset

    A chess dataset is composed by for squares of points. Each square hosts a different number of points, thus each
    square is expected to have a different average density. The points of each square are disposed in an ordered grid.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : ndarray of shape {385,2}, the generated dataset
    labels : {array like} of int, or shape {385}
        Contains the true labels of the generated dataset.
    """
    # blobs with varied variances
    np.random.seed(0)
    random_state = 170
    rows=2
    columns=2
    points=[  5, 8,
            14,10]
    points_number=sum([x*x for x in points])
    data=np.zeros(shape=(points_number,2),dtype=float)
    labels=np.zeros(shape=points_number,dtype=int)
    generated_data=0
    cluster=0
    i=0
    j=0
    for i in range(rows):
        for j in range(columns):
            for ii in [x*1.0/points[columns*i+j] for x in range(points[columns*i+j])]:
                for jj in [x * 1.0 / points[columns * i + j] for x in range(points[columns * i + j])]:
                    data[generated_data]=(i+ii,j+jj)
                    labels[generated_data]=cluster
                    generated_data=generated_data+1
            cluster=cluster+1

    #random è casuale tra 0 e 1
    # (x',y') -< (x+random*0.05,y+random*0.05)
    # (x',y') -< (x+d(x)*random*0.05,y+d(y)*random*0.05)
    # (x',y') -< (x + x*random*0.05,y+y*random*0.05)
    # x,y =1,1
    # x,y= 100,100 <- x' y'= 100+-5,100+-5
    # x',y'= 1+-0.05,1+-0.05

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0,c=labels)
        ax.set_title("ord_chess Dataset")
        fig.show()
    return data,labels

def gen_varied(plot=False):
    """Generates a Varied dataset, the same as the third depicted in scikitlearn examples.
    See: https://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset of shape {1500,2}
    labels : {array like} of int, or shape {1500}
        Contains the true labels of the generated dataset.
    """
    # blobs with varied variances
    np.random.seed(0)
    random_state = 170
    varied,true_label = datasets.make_blobs(n_samples=1500,
                                 cluster_std=[1.0, 2.5, 0.5],
                                 random_state=random_state)
    data=varied
    labels=true_label
    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0,c=labels)
        ax.set_title("Varied Dataset")
        fig.show()
    return data,labels

def gen_aniso(plot=False):
    """Generates an Aniso dataset, the same as the forth depicted in scikitlearn examples.
    See: https://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset of shape {1500,2}
    labels : {array like} of int, or shape {1500}
        Contains the true labels of the generated dataset.
    """
    # blobs with varied variances
    np.random.seed(0)
    random_state = 170
    X, y = datasets.make_blobs(n_samples=1500, random_state=random_state)
    transformation = [[0.6, -0.6], [-0.4, 0.8]]
    X_aniso = np.dot(X, transformation)
    aniso = (X_aniso, y)
    data=X_aniso
    labels=y
    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0,c=labels)
        ax.set_title("Aniso Dataset")
        fig.show()
    return data, labels

def gen_alpha(plot=False):
    """Generates a blobs based, two dimensional dataset with 750 instances.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset of shape {750,2}
    labels : {array like} of int, or shape {750}
        Contains the true labels of the generated dataset.
    """
    centers = [[1, 1], [-1, -1], [5, -5], [5, -5]]

    data, labels_true = make_blobs(n_samples=[150, 150, 150, 300], centers=centers, cluster_std=[0.4, 0.4, 0.4, 0.8],
                                random_state=0)

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0)
        ax.set_title("Alpha Dataset")
        fig.show()
    labels = labels_true
    return data, labels


def gen_bravo(plot=False):
    """Generates another blobs based, two dimensional dataset with 75 instances.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset of shape {75,2}
    labels : {array like} of int, or shape {75}
        Contains the true labels of the generated dataset.
    """
    centers = [[1, 1], [-1, -1], [5, -5], [5, -5]]

    data, labels_true = make_blobs(n_samples=[15, 15, 15, 30], centers=centers, cluster_std=[0.4, 0.4, 0.4, 0.8],
                                random_state=0)

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0)
        ax.set_title("Bravo Dataset")
        fig.show()
    labels = labels_true
    return data, labels


def gen_charlie(plot=False):
    """Generates another blobs based, two dimensional dataset with 750 instances.

    Parameters
    ----------
    plot : boolean
        If the function plots the dataset after generation.

    Returns
    -------
    data : the generated dataset of shape {750,2}
    labels : {array like} of int, or shape {750}
        Contains the true labels of the generated dataset.
    """
    centers = [[1, 1], [-1, -1], [5, -5], [5, -5]]

    data, labels_true = make_blobs(n_samples=[150, 150, 150, 300], centers=centers, cluster_std=[0.4, 0.4, 0.2, 0.4],
                                random_state=0)

    if plot:
        fig, ax = plt.subplots()
        ax.scatter(data[:, 0], data[:, 1], s=1.0)
        ax.set_title("Alpha Dataset")
        fig.show()
    labels = labels_true
    return data, labels


def gen_from_file(filepath="ZahnCompound.csv", plot=False, usecols=(0, 1), labelcol=2):
    """Retrieve a dataset from a .csv file

    Parameters
    ----------
    filepath : str
        Path to the dataset file
    plot :
        If the function is expected to plot the dataset after retrieval.
    usecols : {array like} of int with the indexes of the .csv fields to gather as features
    labelcol : int, index of the .csv field containing the labels.

    Returns
    -------
    data : the retrieved dataset
    labels : {array like} of int, or None
        Contains the labels gathered from the file. If labelcol is None, labels will be None too.
    """
    data = np.loadtxt(filepath, delimiter=",", skiprows=1, usecols=usecols)
    labels = None
    if labelcol is not None:
        labels = np.loadtxt(filepath, delimiter=",", skiprows=1, usecols=labelcol)

    if plot:
        fig, ax = plt.subplots()
        if labelcol is not None:
            ax.scatter(data[:, 0], data[:, 1], s=1.0, c=labels)
        else:
            ax.scatter(data[:, 0], data[:, 1], s=1.0)
        ax.set_title(filepath)
        fig.show()

    return data, labels

