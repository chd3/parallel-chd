import math

from mdc.chd import CHD
from datasetgen import dataset_genAlpha as dg
from mdc import mdc_plot
import matplotlib.pyplot as plt


data, true_labels = dg.gen_from_file("./datasetgen/ZahnCompound.csv")

chd_model=CHD(omega=2.2, k=7, ws=4)
chd_model.fit(data)


plt.figure(0)
plt.scatter(data[:,0],data[:,1])
#mdc_plot.plot_dataset(data,mdcrm_model.labels_)

plt.figure(1)
plt.title("kDistances")
plt.plot(range(len(chd_model.df)),chd_model.df['kdist'])

plt.figure(2)
plt.title("Sorted kDistances")
plt.plot(range(len(chd_model.df)),chd_model.df.sort_values('kdist')['kdist'])

plt.figure(3)
plt.title("Density Variation")
plt.plot(range(len(chd_model.df)),chd_model.df.sort_values('kdist')['denvar'])
#plt.yscale("log")

plt.figure(4)
plt.title("Rolling Mean Density Variation and density level sets threshold")
plt.plot(range(len(chd_model.df)),chd_model.df.sort_values('kdist')['roll_denvar'])
plt.plot(range(len(chd_model.df)),[0+chd_model.threshold]*len(chd_model.df))
#plt.yscale("log")

plt.figure(5)
plt.title("Sorted K Distance and eps value list")
plt.plot(range(len(chd_model.df)), chd_model.df.sort_values('kdist')['kdist'])
for i in range(len(chd_model.epsValues)):
   plt.plot([chd_model.epsValues[i]]*len(chd_model.df))
#plt.yscale("log")


mdc_plot.plot_dataset(data, chd_model.df['dls'],"Density Level Sets")


mdc_plot.plot_dataset(data, chd_model.labels_,"Detected Clusters")


