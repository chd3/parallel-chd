"""Utilities for searching the best model which fits a dataset, through parameter sweeping"""
import pickle

from sklearn import metrics
from sklearn.cluster import DBSCAN
from sklearn.cluster import OPTICS
from mdc.chd import CHD
from mdc.mdcluster import MDCLUSTER
from mdc.mdcluster_core import MDCLUSTER_CORE


def print_scores(data,true_labels,pred_labels,params,name):
    """Print scores for a given dataset and cluster_labels, w.r.t the true_labels of the dataset.

    Parameters
    ----------
    data : arraylike od shape {n,m}, where n are the number of instances and m the number of features. The dataset.
    true_labels : arraylike of int of shape {n}, with the ground truth labels for the dataset.
    pred_labels : arraylike of int of shape {n}, with the cluster labels for a given algorithm.
    params : dictionary of parameters (to be printed) of the algorithm used for predicting the pred_labels
    name : str containing the name of the model to be printed.
    """
    print("Best Model for " + name)
    print(f"with params "+str(params).replace("{","").replace("}","").replace("'",""))
    print(f" Aggregated Score:\t{compute_score(data,true_labels,pred_labels)}")
    print(f"* Fowlkes-Mallows:\t{metrics.fowlkes_mallows_score(true_labels, pred_labels)}")
    print(f"*       Adj. Rand:\t{metrics.adjusted_rand_score(true_labels, pred_labels)}")
    print(f"*             AMI:\t{metrics.adjusted_mutual_info_score(true_labels, pred_labels)}")
    print(f"*       V-mesaure:\t{metrics.v_measure_score(true_labels, pred_labels)}")
    print(f"             Rand:\t{metrics.rand_score(true_labels, pred_labels)}")
    print(f"              NMI:\t{metrics.mutual_info_score(true_labels, pred_labels)}")
    print(f"      Homogeneity:\t{metrics.homogeneity_score(true_labels, pred_labels)}")
    print(f"     Completeness:\t{metrics.completeness_score(true_labels, pred_labels)}")


def compute_score(data,true_labels,pred_labels):
    """Compute an aggregtaed score given the dataset, the ground truth labels and the predicted labels

    Parameters
    ----------
    data : arraylike od shape {n,m}, where n are the number of instances and m the number of features. The dataset.
    true_labels : arraylike of int of shape {n}, with the ground truth labels for the dataset.
    pred_labels : arraylike of int of shape {n}, with the cluster labels for a given algorithm.

    Returns
    -------
    score : float an aggregated score,
        computed as the average of the following indexes: adjusted rand score, adjusted mutual info score,
        v-measure score, and fowlkes-mallows score.
    """
    # score=metrics.adjusted_rand_score(true_labels, mdc.labels_)
    score=0.0
#    score= score+metrics.rand_score(true_labels, pred_labels)
    score= score+metrics.adjusted_rand_score(true_labels, pred_labels)
#    score= score+metrics.mutual_info_score(true_labels, pred_labels)
    score= score+metrics.adjusted_mutual_info_score(true_labels, pred_labels)
#    score= score+metrics.homogeneity_score(true_labels, pred_labels)
#    score= score+metrics.completeness_score(true_labels, pred_labels)
    score= score+metrics.v_measure_score(true_labels, pred_labels)
    score= score+metrics.fowlkes_mallows_score(true_labels, pred_labels)

    return score/4.0

def sweep_mdc_core(data,true_labels,k_values,b_values,c_values, name=None):
    """Compute the best MDCLUSTER_CORE model for a dataset, sweeping the parameters listed in the inputs.

    The model is the one that maximize the score computed in the compute_score() method
    The method will outputs a file named as the "name" input parameter with the best model found.

    Parameters
    ----------
    data : arraylike od shape {n,m}, where n are the number of instances and m the number of features. The dataset.
    true_labels : arraylike of int of shape {n}, with the ground truth labels for the dataset.
    k_values : list of k parameters to be exploited in searching for the best MDCLUSTER_CORE model
    b_values : list of b parameters to be exploited in searching for the best MDCLUSTER_CORE model
    c_values : list of c parameters to be exploited in searching for the best MDCLUSTER_CORE model
    name : str, an arbitrary name for this execution

    Returns
    -------
    model : MDCLUSTER_CORE, the best MDCLUSTER_CORE model fitted.
    params : dict, the parameters of the MDCLUSTER_CORE model fitted.

    """
    if name is None:
        name="mdc_core_sweep"

    # k_values=[2,3,4,5,6,7,8,9,10]
    # b_values=[1.00,1.1,1.2,1.3,1.4,1.50,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0]
    # c_values=[1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#[,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]
    best_mdc=None
    best_score=0
    for k in k_values:
        for b in b_values:
            for c in c_values:
                score=-1
                try:
                    mdc = MDCLUSTER_CORE(k, b, c)
                    mdc = mdc.fit(data)

                    #score=metrics.adjusted_rand_score(true_labels, mdc.labels_)
                    score = compute_score(data,true_labels,mdc.labels_)
                    print(name+f": {k},{b},{c}: {score}")
                    if score > best_score:
                        best_mdc=mdc
                        best_score = score
                        #if(best_score > 0.9):
                        #    print(f"better_found{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")
                        #    pickle.dump(best_mdc, open(f"test_core{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))
                        #    mdc_plot.plot_mdcnet(mdc.data, mdc, title=f"{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")

                except:
                    print(name+f": {k},{b},{c}: {-1}")

    pred_labels=best_mdc.labels_
    params={"k":best_mdc.k,"b":best_mdc.b,"c":best_mdc.c}
    print_scores(data,true_labels,pred_labels,params,name)
    pickle.dump(best_mdc, open(f""+name+f"_{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))

    return best_mdc, params

def sweep_mdc(data,true_labels,k_values,b_values,c_values, name=None):
    """Compute the best MDCLUSTER model for a dataset, sweeping the parameters listed in the inputs.

        The model is the one that maximize the score computed in the compute_score() method
        The method will outputs a file named as the "name" input parameter with the best model found.

        Parameters
        ----------
        data : arraylike od shape {n,m}, where n are the number of instances and m the number of features. The dataset.
        true_labels : arraylike of int of shape {n}, with the ground truth labels for the dataset.
        k_values : list of k parameters to be exploited in searching for the best MDCLUSTER model
        b_values : list of b parameters to be exploited in searching for the best MDCLUSTER model
        c_values : list of c parameters to be exploited in searching for the best MDCLUSTER model
        name : str, an arbitrary name for this execution

        Returns
        -------
        model : MDCLUSTER, the best MDCLUSTER model fitted.
        params : dict, the parameters of the MDCLUSTER model fitted.
    """
    if name is None:
        name="mdc_sweep"

    # k_values=[2,3,4,5,6,7,8,9,10]
    # b_values=[1.00,1.1,1.2,1.3,1.4,1.50,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0]
    # c_values=[1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#[,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]
    best_mdc=None
    best_score=0
    for k in k_values:
        for b in b_values:
            for c in c_values:
                score=-1
                try:
                    mdc = MDCLUSTER(k, b, c)
                    mdc = mdc.fit(data)

                    #score=metrics.adjusted_rand_score(true_labels, mdc.labels_)
                    score = compute_score(data,true_labels,mdc.labels_)
                    print(name+f": {k},{b},{c}: {score}")
                    if score > best_score:
                        best_mdc=mdc
                        best_score = score
                        #if(best_score > 0.9):
                        #    print(f"better_found{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")
                        #    pickle.dump(best_mdc, open(f"test_core{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))
                        #    mdc_plot.plot_mdcnet(mdc.data, mdc, title=f"{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")

                except:
                    print(name+f": {k},{b},{c}: {-1}")

    pred_labels=best_mdc.labels_
    params={"k":best_mdc.k,"b":best_mdc.b,"c":best_mdc.c}
    print_scores(data,true_labels,pred_labels,params,name)
    pickle.dump(best_mdc, open(f""+name+f"_{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))

    return best_mdc, params

def sweep_dbscan(data,true_labels,eps_values,min_samples_values, name=None):
    """Compute the best DBSCAN model for a dataset, sweeping the parameters listed in the inputs.

    The model is the one that maximize the score computed in the compute_score() method
    The method will outputs a file named as the "name" input parameter with the best model found.

    Parameters
    ----------
    data : arraylike od shape {n,m}, where n are the number of instances and m the number of features. The dataset.
    true_labels : arraylike of int of shape {n}, with the ground truth labels for the dataset.
    eps_values : list of eps parameters to be exploited in searching for the best DBSCAN model
    min_samples_values : list of min_samples parameters to be exploited in searching for the best DBSCAN model
    name : str, an arbitrary name for this execution

    Returns
    -------
    model : DBSCAN, the best DBSCAN model fitted.
    params : dict, the parameters of the DBSCAN model fitted.
    """
    if name is None:
        name="dbscan_sweep"

    # k_values=[2,3,4,5,6,7,8,9,10]
    # b_values=[1.00,1.1,1.2,1.3,1.4,1.50,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0]
    # c_values=[1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#[,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]
    best_model=None
    best_score=0
    for eps in eps_values:
        for min_samples in min_samples_values:
            try:
                model = DBSCAN(eps=eps,min_samples=min_samples)
                model = model.fit(data)

                score = compute_score(data,true_labels,model.labels_)
                print(name+f": {eps},{min_samples}: {score}")
                if score > best_score:
                    best_model=model
                    best_score = score
                    #if(best_score > 0.9):
                    #    print(f"better_found{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")
                    #    pickle.dump(best_mdc, open(f"test_core{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))
                    #    mdc_plot.plot_mdcnet(mdc.data, mdc, title=f"{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")

            except:
                print(name+f": {eps},{min_samples}: {-1}")

    pred_labels=best_model.labels_
    params={"eps":best_model.eps,"min_samples":best_model.min_samples}
    print_scores(data,true_labels,pred_labels,params,name)
    pickle.dump(best_model, open(f""+name+f"_{best_model.eps}_{best_model.min_samples}.pickle", "wb"))

    return best_model, params

def sweep_optics(data,true_labels,xi_values,min_samples_values, name=None):
    """Compute the best optics-xi model for a dataset, sweeping the parameters listed in the inputs.

        The model is the one that maximize the score computed in the compute_score() method
        The method will outputs a file named as the "name" input parameter with the best model found.

        Parameters
        ----------
        data : arraylike od shape {n,m}, where n are the number of instances and m the number of features. The dataset.
        true_labels : arraylike of int of shape {n}, with the ground truth labels for the dataset.
        min_samples_values : list of min_samples parameters to be exploited in searching for the best optics-xi model
        xi_values : list of xi parameters to be exploited in searching for the best optics-xi model
        name : str, an arbitrary name for this execution

        Returns
        -------
        model : OPTICS, the best optics-xi model fitted.
        params : dict, the parameters of the optics-xi model fitted.
        """
    if name is None:
        name="optics_sweep"

    # k_values=[2,3,4,5,6,7,8,9,10]
    # b_values=[1.00,1.1,1.2,1.3,1.4,1.50,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0]
    # c_values=[1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]#[,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5]
    best_model=None
    best_score=0

    for min_samples in min_samples_values:
        for xi in xi_values:
            try:
                model = OPTICS(min_samples=min_samples,xi=xi,metric='euclidean')
                model = model.fit(data)

                score = compute_score(data,true_labels,model.labels_)
                print(name+f": {xi},{min_samples}: {score}")
                if score > best_score:
                    best_model=model
                    best_score = score
                    #if(best_score > 0.9):
                    #    print(f"better_found{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")
                    #    pickle.dump(best_mdc, open(f"test_core{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))
                    #    mdc_plot.plot_mdcnet(mdc.data, mdc, title=f"{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")

            except:
                print(name+f": {xi},{min_samples}: {-1}")

    pred_labels=best_model.labels_
    params={"xi":best_model.xi,"min_samples":best_model.min_samples}
    print_scores(data,true_labels,pred_labels,params,name)
    pickle.dump(best_model, open(f""+name+f"_{best_model.xi}_{best_model.min_samples}.pickle", "wb"))

    return best_model, params

def sweep_mdcrollmean(data,true_labels,omega_values,k_values, ws_values, name=None):

    if name is None:
        name="mdcrollmean_sweep"

    best_model=None
    best_score=0

    for omega in omega_values:
        for k in k_values:
            for ws in ws_values:
                try:
                    model = CHD(omega=omega, k=k, ws=ws)
                    model = model.fit(data)

                    score = compute_score(data,true_labels,model.labels_)
                    print(name+f": {omega},{k},{ws}: {score}")
                    if score > best_score:
                        best_model=model
                        best_score = score
                        #if(best_score > 0.9):
                        #    print(f"better_found{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")
                        #    pickle.dump(best_mdc, open(f"test_core{best_mdc.k}_{best_mdc.b}_{best_mdc.c}.pickle", "wb"))
                        #    mdc_plot.plot_mdcnet(mdc.data, mdc, title=f"{best_mdc.k},{best_mdc.b},{best_mdc.c}: {best_score}")

                except:
                    print(name+f": {omega},{k},{ws}: {-1}")

    pred_labels=best_model.labels_
    params={"omega":best_model.omega,"k":best_model.k,"ws":best_model.ws}
    print_scores(data,true_labels,pred_labels,params,name)
    pickle.dump(best_model, open(f""+name+f"_{best_model.omega}_{best_model.k}_{best_model.ws}.pickle", "wb"))

    return best_model, params