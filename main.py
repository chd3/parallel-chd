from dask.distributed import Client, LocalCluster
import pandas as pd
from mdc import chd
import warnings
warnings.filterwarnings("ignore")

if __name__ == '__main__':
    cluster=LocalCluster()
    client=Client(cluster)

    df = pd.read_csv('./datasetgen/ZahnCompound.csv')
    data = df.iloc[:,:2]
    print(len(data))
    omega = 2.5
    k = 4
    ws = 1

    model = chd.CHD(omega, k, ws)
    model = model.fit(data)

    print(model.labels_)