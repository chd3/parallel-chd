import datetime
import time
import math
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN
from itertools import combinations
import warnings
import joblib
import warnings
warnings.filterwarnings("ignore")




class CHD:
    def __init__(self, omega, k, ws, n_parts = 10):
        self.omega=omega #omega parameter
        self.k = k # kvalue
        self.ws = ws #window size for rollmean
        self.n_parts = n_parts
        #self.kdistance=None
        self.labels_=None
        self.data=None
        self.df=None
        self.epsValues=None
        self.threshold=None
        

    @staticmethod
    def runDBScanInstances(data, dls_eps_list, minPoints, columns=[0,1], densityVarMode="rolling"):
        currInitialClusterId = 0
        currentLabels = pd.DataFrame(None)
        for i in range(len(dls_eps_list)):
            currentEPS = dls_eps_list[i]
            if math.isnan(currentEPS):#ATTENZIONE
                warnings.warn("Warning: the {0}th epsilon is nan, the next one is used.".format(str(i)))
                currentEPS = dls_eps_list[i+1]#ATTENZIONE
            currentDLSdata = data[data['dls'] == i].iloc[:, [0, 1]]
            model = DBSCAN(eps=currentEPS, min_samples=minPoints)
            model = model.fit(currentDLSdata)
            currentDBSResult = model.labels_
            for j in range(len(currentDBSResult)):
                if currentDBSResult[j] > -1:
                    currentDBSResult[j] = currentDBSResult[j] + currInitialClusterId
            currentDLSdata['cluster'] = currentDBSResult
            currentLabels = pd.concat([currentLabels, currentDLSdata])
            #print(i)
            #print(dls_eps_list[i])
            #print(currInitialClusterId)
            #print(max(currentDBSResult))
            #print(max(0,max(currentDBSResult)))
            #currInitialClusterId = currInitialClusterId+max(currentDBSResult) + 1
            currInitialClusterId =  max(currInitialClusterId,max(currentDBSResult) + 1)
            #print(currInitialClusterId)
            #print("------")
        return currentLabels['cluster'].sort_index()

    @staticmethod
    def runDBSCAN(data, eps, min_pts):
        dbscan = DBSCAN(eps=eps, min_samples=min_pts)
        start = time.time_ns() // 1000000
        model = dbscan.fit(data)
        end = time.time_ns() // 1000000
        t = '{:.4f}ms'.format((end - start))
        result = (model.labels_, t)
        return result

    @staticmethod
    def combineDataEps(data, epsValues):
        data_eps_list = []
        for i in range(len(epsValues)):
            currentEPS = epsValues[i]
            currentDLSdata = data[data['dls'] == i].iloc[:, [0, 1]]
            data_eps_list.append((currentDLSdata, currentEPS))
        return data_eps_list

    @staticmethod
    def mergeDBSCANresult(data, result):
        currInitialClusterId = 0
        currentLabels = pd.DataFrame(None)
        for i in range(len(result)):
            currentDLSdata = data[data['dls'] == i].iloc[:, [0, 1]]
            currentDBSResult = result[i][0]
            for j in range(len(currentDBSResult)):
                if currentDBSResult[j] > -1:
                    currentDBSResult[j] = currentDBSResult[j] + currInitialClusterId
            currentDLSdata['cluster'] = currentDBSResult
            currentLabels = pd.concat([currentLabels, currentDLSdata])
            currInitialClusterId = max(currInitialClusterId, max(currentDBSResult) + 1)
        return currentLabels['cluster'].sort_index()

    @staticmethod
    def ComputeDensityVariationTER(kDistances):
        denvar = np.zeros(len(kDistances))
        for i in range(len(kDistances) - 1):
            if (kDistances[i + 1] == 0):
                denvar[i] = 0
            else:
                denvar[i] = 2 * (kDistances[i + 1] - kDistances[i]) / (kDistances[i + 1] + kDistances[i])
        denvar[len(kDistances) - 1] = 0
        #plt.plot(denvar)
        return denvar

    @staticmethod
    def computeDensityLevelSets(densityVariation, thresholdValue):
        curr_dls_id = 0
        inputVector = densityVariation
        inputThrd = thresholdValue
        outputVector = np.zeros(len(inputVector))
        firstAbove = False
        for i in range(len(inputVector)):
            if i == 0:
                curr_dls_id = 0
                outputVector[i] = curr_dls_id
                if (inputVector[i] <= inputThrd):
                    firstAbove = False
                else:
                    firstAbove = True
            elif inputVector[i] <= inputThrd:
                outputVector[i] = curr_dls_id
                firstAbove = False
            elif inputVector[i] > inputThrd:
                if (firstAbove == False):
                    firstAbove = True
                    curr_dls_id = curr_dls_id + 1
                outputVector[i] = curr_dls_id
        return outputVector

    @staticmethod
    def computeEpsValues_RollMean(dls, kdist):
        num_dls = int(max(dls) + 1)
        dls_ids = [0] * (num_dls)
        epsValues = [0.0] * (num_dls)
        for i in range(num_dls):
            kDistances = [val for is_good, val in zip(dls, kdist) if int(is_good) == i]
            maxkdist = max(kDistances)
            mediankdist = np.median(kDistances)
            meankdist = np.mean(kDistances)
            epsValue = maxkdist * math.sqrt(mediankdist / meankdist)
            dls_ids[i] = i
            epsValues[i] = epsValue
        return dls_ids, epsValues
    
    @staticmethod
    def create_subpartition(data, num_partitions):
        group = pd.cut(data.index, bins=num_partitions, labels=False)
        # Group the DataFrame by the partitions
        partitions = data.groupby(group)
        ind = [i for i, p in partitions]
        part_len = [len(p) for i, p in partitions]
        comb = list(combinations(ind, 2))
        df_list = []
        for c in comb:
            df1 = partitions.get_group(c[0])
            df2 = partitions.get_group(c[1])
            dc = pd.concat([df1, df2])
            df_list.append((dc, c))
        return df_list, part_len
    
    @staticmethod
    def runKNN(data, k, comb):
        neigh = NearestNeighbors(n_neighbors=(k), algorithm = 'brute')
        start = time.time_ns()//1000000    
        neigh.fit(data)
        end = time.time_ns()//1000000
        t = '{:.4f}ms'.format((end - start))
        dist = neigh.kneighbors()[0]
        ind = neigh.kneighbors()[1]
        d_i = []
        for i in range(len(dist)):
            l_ind = []
            for j in range(len(dist[i])):
                l_ind.append((dist[i][j],data.index[ind[i][j]]))
            d_i.append(l_ind)
        #d_i = np.array(d_i)
        dist_ind = (d_i, comb, t)
        #dist_ind = (dist, ind, comb, t)
        return dist_ind
    
    @staticmethod
    def merge_kdist(result, n_parts, n_dist, part_len):
        dists = []
        ind = []

        for par in range(n_parts):
            # per ciascuna partizione viene costruita una lista di distanze calcolate su ciascuna combinazione
            # ES: per la partizione 0 vengono prese le prime k distanze di ciascun punto appartenente ad essa calcolate nelle varie combinazioni (0,1), (0,2), ..., (0,n°part)
            part_list = []

            for elem in result:
                combination_index = elem[1]  # tupla contenente gli indici delle partizioni combinate sulla quale è stata calcolata la k-dist
                partition_index1 = combination_index[0]  # indice 1 della combinazione
                partition_index2 = combination_index[1]  # indice 2 della combinazione
                distances_points_matrix = elem[0]  # matrice distanze-punti relativa a ciascuna combinazione di partizioni
                part_len_index1 = part_len[partition_index1]  # lunghezza partizione 1
                part_len_index2 = part_len[partition_index2]  # lunghezza partizione 2

                if par == partition_index1:
                    dat = distances_points_matrix[:part_len_index1]
                    part_list.append(dat)
                if par == partition_index2:
                    dat = distances_points_matrix[-part_len_index2:]
                    part_list.append(dat)

            # per ciascun punto vengono create due liste ordinate: una contenente le distanze e una contenente gli indici dei punti per cui è stata calcolata la distanza presente in dist
            for j in range(part_len[par]):
                point_dist = list(part_list[0][j])
                for p in range(1, len(part_list)):
                    for i in range(len(part_list[p][j])):
                        if part_list[p][j][i] not in point_dist:
                            point_dist.append(part_list[p][j][i])
                point_dist = sorted(point_dist, key=lambda x: x[0])
                pts_dist = [x[0] for x in point_dist]
                pts_ind = [x[1] for x in point_dist]
                dists.append(pts_dist[:n_dist])
                ind.append(pts_ind[:n_dist])
                #print(point_dist)
        dists = np.array(dists)
        ind = np.array(ind)
        return dists, ind

    def fit(self, data):
        self.data=data
        df = pd.DataFrame(data)
        self.df = df
        df_list, part_len = self.create_subpartition(df, self.n_parts)
        #plt.figure(0)
        #plt.scatter(df[0], df[1])
        
        with joblib.parallel_backend(backend="dask"): 
            result = joblib.Parallel(verbose=100)(
            joblib.delayed(self.runKNN)(data, self.k, c) for data, c in df_list
        )
        
        dists, inds = self.merge_kdist(result, self.n_parts, self.k, part_len)

        #neigh = NearestNeighbors(n_neighbors=(self.k))
        #neigh.fit(df)
        #plt.figure(1)
        #plt.plot(neigh.kneighbors()[0])
        self.df = df

        #kdist = [row[self.k-1] for row in neigh.kneighbors()[0]]
        kdist = [row[self.k-1] for row in dists]
        
        #plt.figure(2)
        #plt.plot(kdist)
        df['kdist'] = kdist

        sdf = df.sort_values('kdist')
        #plt.plot(range(len(sdf['kdist'])), sdf['kdist'])

        denvar = self.ComputeDensityVariationTER(sdf['kdist'].to_numpy())
        sdf['denvar'] = denvar

        # applichiamo la rolling mean (attenzione che da alcuni valori NaN
        sdf['roll_denvar'] = sdf['denvar'].rolling(self.ws).mean()

        #plt.figure(3)
        #plt.plot(range(len(sdf['roll_denvar'])), sdf['roll_denvar'])

        thrd_rollmean = sdf['roll_denvar'].mean() + self.omega * math.sqrt(sdf['roll_denvar'].var())
        #plt.plot(np.ones(len(sdf['roll_denvar'])) * thrd_rollmean)

        rolldenvar = sdf['roll_denvar'].to_numpy()
        dls = self.computeDensityLevelSets(densityVariation=rolldenvar, thresholdValue=thrd_rollmean)
        sdf['dls'] = dls
        dls_ids, epsValues = self.computeEpsValues_RollMean(dls, sdf['kdist'].to_numpy())
        self.epsValues = epsValues
        #plt.figure(4)
        #plt.plot(range(len(sdf['kdist'])), sdf['kdist'])
        #for i in range(len(epsValues)):
        #    plt.plot(np.ones(len(kdist)) * epsValues[i])

        df = sdf.sort_index()
        self.df = df
        #plt.figure(5)
        #mdc_plot.plot_dataset(df.iloc[:, [0, 1]].to_numpy(), df['dls'])

        data_eps_list = self.combineDataEps(df, epsValues)

        with joblib.parallel_backend(backend="dask"):
            result = joblib.Parallel(verbose=100)(
            joblib.delayed(self.runDBSCAN)(currData, currEps, self.k) for currData, currEps in data_eps_list
        )

        clusters = self.mergeDBSCANresult(df, result)

        #clusters = self.runDBScanInstances(df, epsValues, self.k, columns=[*range(data.shape[1])])


        df['clusters'] = clusters

        #plt.figure(6)
        #mdc_plot.plot_dataset(df.iloc[:, [0, 1]].to_numpy(), df['clusters'])
        self.labels_=df['clusters'].to_numpy()
        self.df=df
        self.epsValues=epsValues
        self.threshold=thrd_rollmean
        return self


