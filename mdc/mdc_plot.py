"""Utilities for plotting clustering results"""
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors


def normalize_labels(labels):
    """Normalize labels in int from 0 to N, where N is the number of different labels.

    Parameters
    ----------
    labels : {array like} of int representing clustering labels

    Returns
    -------
    labels_norm : {array like} of int with the new normalized labels
    """
    plabel = list(set(labels))
    pcolor = list(range(0, len(plabel)))
    clabel = list(pcolor[plabel.index(x)] for x in labels)
    return clabel


def plot_dataset(data, labels=None, title=None):
    """Plots a dataset data, colored by the given labels

    Parameters
    ----------
    data : {array-like} of shape (n_samples, n_features)
    labels : int array, shape = (``n_samples``,)
        A clustering of the data into disjoint subsets.
    title : str, the Title of the resulting plot
    """
    color_list = list(mcolors.TABLEAU_COLORS.keys())
    clabels = normalize_labels(labels)
    colors = list(color_list[x % 10] for x in clabels)
    fig, ax = plt.subplots()
    if title is not None:
        ax.set_title(title)
    clabels = normalize_labels(labels)
    ax.set_ylabel("feature 1")
    ax.set_xlabel("feature 0")
    ax.scatter(data.iloc[:, 0], data.iloc[:, 1], s=1.0, c=colors)

    fig.show()


def plot_mdcnet(data, mdc, title=None):
    """Plots a MULTIDENSITY points network, given a dataset.

    Parameters
    ----------
    data : {array-like} of shape (n_samples, n_features)
    mdc : MDCLUSTER or MDCLUSTER_CORE object, previously fitted with a call to fit()
    title : str, the Title of the resulting plot
    """
    color_list = list(mcolors.TABLEAU_COLORS.keys())
    clabels = normalize_labels(mdc.labels_)
    colors = list(color_list[x % 10] for x in clabels)
    fig, ax = plt.subplots()
    for i in range(0, len(data)):
        if hasattr(mdc,"_ifcores"):
            if mdc._ifcores[i] == 1:
                circle = plt.Circle(data[i], 0.05*mdc.b * mdc.avg_kdist[i], fc='none', ec=colors[i], alpha=0.9)
                ax.add_patch(circle)

        circle = plt.Circle(data[i], mdc.b * mdc.avg_kdist[i], fc='none', ec=colors[i], alpha=0.05)
        ax.add_patch(circle)

        # direct=[dest[0]-X[i][0],dest[1]-X[i][1]]
        # arrow = plt.Arrow(X[i][0],X[i][1], direct[0],direct[1],width=0.01,color=colors[i],alpha=0.5)
    #   ax.add_patch(circle)
    if hasattr(mdc, 'links'):
        if len(mdc.links[0])==2:
            for i, j in mdc.links:
                ax.plot([data[i][0], data[j][0]], [data[i][1], data[j][1]], c=colors[i], linewidth=0.5)
                # circle = plt.Circle(data[i], mdc.b * mdc.avg_kdist[i], fc='none', ec=colors[i], alpha=0.1)
                # ax.add_patch(circle)
                circle = plt.Circle(data[j], mdc.b * mdc.avg_kdist[j], fc='none', ec=colors[j], alpha=0.1)
                ax.add_patch(circle)
        else:
            for i, j,t in mdc.links:
                if t==0:
                    ax.plot([data[i][0], data[j][0]], [data[i][1], data[j][1]], c=colors[i], linewidth=0.5)
                if t==1:
                    ax.plot([data[i][0], data[j][0]], [data[i][1], data[j][1]], c=colors[i], linewidth=0.1)
                # circle = plt.Circle(data[i], mdc.b * mdc.avg_kdist[i], fc='none', ec=colors[i], alpha=0.1)
                # ax.add_patch(circle)
                #circle = plt.Circle(data[j], mdc.b * mdc.avg_kdist[j], fc='none', ec=colors[j], alpha=0.1)
                #circle = plt.Circle(data[i], mdc.b * mdc.avg_kdist[i], fc='none', ec=colors[i], alpha=0.1)
                #ax.add_patch(circle)
        # ax.add_patch(arrow)
    if title is not None:
        ax.set_title(title)
    ax.set_ylabel("feature 1")
    ax.set_xlabel("feature 0")
    ax.scatter(data[:, 0], data[:, 1], s=1.0, c=colors)
    fig.show()


def plot_avgkdists(data, mdc, title=None):
    """Plots the sorted avg_kdist of the points in each cluster  given an MDCLUSTER

    Parameters
    ----------
    data : {array-like} of shape (n_samples, n_features)
    mdc : MDCLUSTER or MDCLUSTER_CORE object, previously fitted with a call to fit()
    title : str, the Title of the resulting plot
    """
    color_list = list(mcolors.TABLEAU_COLORS.keys())
    clabels = normalize_labels(mdc.labels_)
    colors = list(color_list[x % 10] for x in clabels)

    fig, ax = plt.subplots()
    indexes = range(len(mdc.avg_kdist))
    indexes = sorted(indexes, key=lambda x: (mdc.labels_[x], mdc.avg_kdist[x]))
    dat = [mdc.avg_kdist[i] for i in indexes]
    col = [colors[i] for i in indexes]
    if title is not None:
        ax.set_title(title)
    ax.set_ylabel("average kdist")
    ax.set_xlabel("data points")
    ax.scatter(range(len(mdc.avg_kdist)), dat, c=col, s=1.0)
    fig.show()

def plot_a_thing(data, mdc, thing,title=None):
    """Plots things.. this method is not yet finalized, so please don't use it.

    Parameters
    ----------
    data : {array-like} of shape (n_samples, n_features)
    mdc : MDCLUSTER or MDCLUSTER_CORE object, previously fitted with a call to fit()
    thing : a thing to be plotted
    title : str, the Title of the resulting plot
    """
    color_list = list(mcolors.TABLEAU_COLORS.keys())
    clabels = normalize_labels(mdc.labels_)
    colors = list(color_list[x % 10] for x in clabels)

    fig, ax = plt.subplots()
    indexes = range(len(thing))
    indexes = sorted(indexes, key=lambda x: (mdc.labels_[x], thing[x]))
    dat = [thing[i] for i in indexes]
    col = [colors[i] for i in indexes]
    if title is not None:
        ax.set_title(title)
    ax.set_ylabel("average kdist")
    ax.set_xlabel("data points")
    ax.scatter(range(len(thing)), dat, c=col, s=1.0)
    fig.show()