# parallel-chd
This is a parallel implementation of the City Hotspot Detector algorithm, written in Python, as described in the paper:

[Eugenio Cesario, Paolo Lindia, Andrea Vinci:\
A Scalable Multi-density Clustering Approach to detect City Hotspots in a Smart City\
*Submitted to* Elsevier Future Generation Computing Systsmes in 2023 and currently under review.](https://www.sciencedirect.com/journal/future-generation-computer-systems)

# Disclaimer
The project is available "as is" only for testing.
The project is currently under development and further testing and refinement is required before its use in production environment.
